set nocompatible
filetype off

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'VundleVim/Vundle.vim'
Plugin 'ycm-core/YouCompleteMe'
Plugin 'flazz/vim-colorschemes'

call vundle#end()

filetype plugin indent on

let g:ycm_extra_conf_globlist = ['~/budget_sf/*']

syntax on
filetype on
colorscheme torte
set listchars=eol:$,tab:>-,trail:~,extends:>,precedes:<
set list

set shiftwidth=4
set tabstop=4
set noexpandtab
set number
set completeopt-=preview
set splitright
set splitbelow
set ff=unix
set wildmode=list:longest

filetype plugin indent on
