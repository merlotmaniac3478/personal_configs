#!/bin/bash -e
#Script to configure, make, and install vim

usage() {
	echo "Usage:  "
	echo "-i: install location"
	echo "-r: runtime location"
	echo "-d: source location"
	echo "-u: uninstall vim"
	echo "-c: custom configuration options"
	echo "-a: additional configuration options"
	echo "-h: display this message"
}

#Default directories
SRC_DIR=$HOME/build/vim
RUNTIME_DIR=/usr/local/share/vim/vim82
INSTALL_DIR=/usr/local
AUTOCONF_CACHE=src/auto/config.cache
CONFIG_OPTS=" --with-features=huge \
	--enable-multibyte \
	--enable-python3interp=yes \
	--with-python3-command=python3.10 \
	--with-python3-config-dir=$(python3.10-config --configdir) \
	--disable-gui \
	--without-x \
	--disable-gpm \
	--disable-xsmp \
	--enable-cscope \
	--enable-luainterp \
	--with-lua-prefix=/usr \
	--enable-largefile \
"
UNINTALL=0

while getopts ":i:r:d:c:a:h:u" o; do
	case "${o}" in
		i)
			INSTALL_DIR="${OPTARG}"
			echo "Using user defined install location: $INSTALL_DIR"
			;;
		r)
			RUNTIME_DIR=${OPTARG}
			echo "Using user defined install location: $RUNTIME_DIR"
			;;
		d)
			VIM_DIR=${OPTARG}
			echo "Using user defined source directory: $SRC_DIR"
			;;
		c)
			CONFIG_OPTS=${OPTARG}
			echo "Using user defined configuration options:\n$CONFIG_OPTS"
			;;
		a)
			ADDITIONAL_CONFIG=${OPTARG}
			echo "Using additional configuration options:\n$ADDITIONAL_CONFIG"
			;;
		h)
			usage
			exit 0
			;;
		u)
			UNINSTALL=1
			echo "Uninstalling VIM"
			;;
		*)
			;;
	esac
done

if [[ $UNINSTALL -eq 1 ]]; then
	cd $SRC_DIR
	echo "Uninstalling vim"
	su -c 'make uninstall'
	echo "Uninstall complete"
	exit 0
fi

cd $SRC_DIR
echo "Removing autoconf cache"
rm -v $AUTOCONF_CACHE || true

echo "Cleaning"
make clean


echo "Configuring VIM with config options: $CONFIG_OPTS"
echo "Additional options: $ADDITIONAL_OPTS"
echo "And installation directory: $INSTALL_DIR"

./configure $CONFIG_OPTS $ADDITIONAL_CONFIG --prefix=$INSTALL_DIR

make VIMRUNTIMEDIR=$RUNTIME_DIR -j$(cat /proc/cpuinfo | grep processor | wc -l)

su -c 'make install'

echo "Install complete"
